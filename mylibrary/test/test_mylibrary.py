import pytest
import mylibrary
def test_add():
    assert mylibrary.add_numbers(2, 3) == 5
    assert mylibrary.add_numbers(-1, 1) == 0
    assert mylibrary.add_numbers(0, 0) == 0
    assert mylibrary.add_numbers(-1, -1) == -2
