"""Tests the math function"""
import pytest
import sys
sys.path.append("/mylibrary/src/*.py")
import mylibrary

def test_add():
    """Test add function"""
    assert mylibrary.add_numbers(2, 3) == 5
    assert mylibrary.add_numbers(-1, 1) == 0
    assert mylibrary.add_numbers(-1, -1) == -2
    assert mylibrary.add_numbers(0, 0) == 0

def test_multiply():
    """Test multiply function"""
    assert mylibrary.multiply_numbers(2, 3) == 6
    assert mylibrary.multiply_numbers(-1, 1) == -1
    assert mylibrary.multiply_numbers(-1, -1) == 1
    assert mylibrary.multiply_numbers(0, 0) == 0
