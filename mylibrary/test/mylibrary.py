"""Docstring"""
import numpy as np
def add_numbers(num_a, num_b):
    """ add_numbers():sum of two numbers 
    num_a: first number
    num_b: second number """
    return num_a + num_b
def multiply_numbers(num_a, num_b):
    """ multiply_numbers():multiply of two numbers 
    num_a: first number
    num_b: second number """
    return num_a * num_b
def add_arrays(num_a, num_b):
    """ add_arrays():add of two arrays
    num_a: first number
    num_b: second number """
    return np.add(num_a, num_b)
