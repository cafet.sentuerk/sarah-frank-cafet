"""My docstrıng"""
import numpy as np
import mylibrary

def main():
    """My docstrıng"""
    result = mylibrary.add_numbers(2, 3)
    print("2 + 3 = ", result)

    result = mylibrary.multiply_numbers(2558, 374)
    print("2558 * 374 = ", result)

    array_a = np.array([1, 2])
    array_b = np.array([3, 4])
    array_c = mylibrary.add_arrays(array_a, array_b)

    print(array_c)
